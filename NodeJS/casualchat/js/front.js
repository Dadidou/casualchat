// import { Push } from 'push.js';

$(function () {
    const socket = io();  // io() provides a connection to the host that serves the page, so don't need srv address
    let nickname = prompt('What\'s your name ?');
    let msgList = $('#messages');
    let input = $('#input');

    if (nickname === null || nickname === '') {
        nickname = 'Anonymous';
    }
    socket.emit('enter chat', nickname);
    // Push notification when enter chat
    Push.create("You entered in the chat", {
        timeout: 3000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });

    // when entered to the chat, receive welcome message with nbr of connected users
    socket.on('welcome message', function (nbUsers) {
        const li = $('<li class="welcome-msg-container">');
        const msgAlone = $('<strong class="welcome-msg">').text(`Welcome ${nickname}, you're alone in the chat`);
        const msgOne = $('<strong class="welcome-msg">').text(`Welcome ${nickname}, there is one other user in the chat`);
        const msgMany = $('<strong class="welcome-msg">').text(`Welcome ${nickname}, there are ${nbUsers - 1} other users in the chat`);

        // Just says welcome after entering chat.
        msgList.append(li);
        // Giving the number of other users following cases :
        switch (nbUsers) {
            case 1:
                li.append(msgAlone);
                break;
            case 2:
                li.append(msgOne);
                break;
            default:
                li.append(msgMany);
                break;
        }
    });

    // when submit form
    $('form').submit(function () {
        const message = input.val();
        // send a message to the srv.
        socket.emit('chat message', message);
        //---------------------------------------------------------------------------------
        // The owner message will shows automaticaly and looks different from other messages
        const li = $('<li class="own-msg-container">');
        const nick = $('<strong class="own-nickname">').text(`${nickname} : `);
        const msg = $('<span class="own-msg">').text(`${message}`);

        msgList.append(li.append(nick).append(msg));    // Append directly the message to style it diferently (because "owner message")
        msgList.scrollTop(msgList.prop('scrollHeight'));      //Scroll to bototm
        // clean input
        input.val('');
        // exit
        return false;
    });

    // when receive a message, display it and display the sender
    socket.on('chat message', function (data) {
        // Push notification when a message comes
        Push.create(data.nickname, {
            body: data.message,
            timeout: 3000,
            onClick: function () {
                window.focus();
                this.close();
            }
        });

        const li = $('<li class="msg-container">');
        const nick = $('<strong class="nickname">').text(`${data.nickname} : `);
        const msg = $('<span class="msg">').text(`${data.message}`);

        msgList.append(li.append(nick).append(msg)); // Construct a message with nickname and message from the object data passed in emit from srv
        msgList.scrollTop(msgList.prop('scrollHeight'));  //Scroll to bottom
    });

    socket.on('info message enter', function (nickname) {
        const li = $('<li class="info-msg-container">');
        const nick = $('<strong class="info-nickname">').text(`${nickname} `);
        const msg = $('<span class="info-msg">').text('joins the chat');

        msgList.append(li.append(nick).append(msg));
        msgList.scrollTop(msgList.prop('scrollHeight'));
    });

    socket.on('info message leave', function (nickname) {
        const li = $('<li class="info-msg-container">');
        const nick = $('<strong class="info-nickname">').text(`${nickname} `);
        const msg = $('<span class="info-msg">').text('leaves the chat');

        msgList.append(li.append(nick).append(msg));
        msgList.scrollTop(msgList.prop('scrollHeight'));
    });

});