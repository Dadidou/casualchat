const express = require('express'); // Get the module
const app = express(); // Create express by calling the prototype in var express
const fs = require('fs');
const https = require('https');
const options = {
    key: fs.readFileSync('cert/key.pem'),
    cert: fs.readFileSync('cert/cert.pem'),
    passphrase: 'yoloo'
};
var server = https.createServer(options, app);
var io = require('socket.io')(server);

const port = process.env.PORT || 3000;

// ---------------------expressJS-------------------------
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/js/front.js', function (req, res) {
    res.sendFile(__dirname + '/js/front.js');
});

app.use(express.static('js'));
app.use(express.static('assets'));     // assets = static folder (img, css, etc..)
// -------------------------------------------------------

let nbUsers = 0;

// create a socket when co to the serv
io.on('connection', function (socket) {
    console.log('socket connected');

    // we get the name of user to do things into his "session"
    socket.on('enter chat', function (nickname) {
        nbUsers++;

        console.log('nbUsers : ' + nbUsers);

        // send welcome and nb users when somebody comes
        socket.emit('welcome message', nbUsers);

        // we say to other people 'bidule is here !" We just need nickname
        socket.broadcast.emit('info message enter', nickname);

        // when receive a discussion message from everyone, we broadcast it to all other connections (and not to the current connection because the owner's message looks diferent to himself)
        socket.on('chat message', function (msg) {
            socket.broadcast.emit('chat message', {nickname: nickname, message: msg});    // we send the nickname on the connection and a message ("Truc : yo guys !")
        });

        socket.on('disconnect', function () {
            console.log('socket disconnected');

            // we say to other people 'bidule is gone !" We just need nickname
            socket.broadcast.emit('info message leave', nickname);

            nbUsers--;
        });
    });
});


server.listen(port, function () {
    console.log('listening on *:' + port);
});


